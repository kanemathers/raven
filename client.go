package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"net"
	"strings"
)

type Handler func(*IRCClient, Message)

type IRCClient struct {
	hostname   string
	port       int
	connection net.Conn
	reader     *bufio.Reader
	handlers   map[string][]Handler
	modules    map[string]Module
}

type Message struct {
	raw     string
	prefix  string
	command string
	args    []string
}

func NewClient() (*IRCClient, error) {
	client := &IRCClient{
		handlers: make(map[string][]Handler),
		modules:  make(map[string]Module),
	}

	return client, nil
}

func (client *IRCClient) LoadModules(modules []string) error {
	for _, module := range modules {
		if m := LoadModule(module); m != nil {
			log.Printf("loading module: %s\n", module)

			client.modules[module] = m
			m.Init(client)
		} else {
			log.Printf("no such module: %s\n", module)
		}
	}

	return nil
}

func (client *IRCClient) Connect(hostname string, port int) error {
	client.hostname = hostname
	client.port = port
	uri := fmt.Sprintf("%s:%d", hostname, port)

	if connection, err := net.Dial("tcp", uri); err == nil {
		client.connection = connection
	} else {
		return err
	}

	log.Printf("connected to %s:%d\n", hostname, port)

	client.reader = bufio.NewReader(client.connection)

	client.Dispatch("CONNECTED", Message{})

	return nil
}

func (client *IRCClient) Disconnect() {
	log.Printf("disconnecting from %s:%d\n", client.hostname, client.port)

	client.connection.Close()
}

func (client *IRCClient) AddHandler(command string, fn Handler) {
	client.handlers[command] = append(client.handlers[command], fn)
}

func (client *IRCClient) Dispatch(command string, message Message) {
	for _, handler := range client.handlers[command] {
		handler(client, message)
	}
}

func (client *IRCClient) Loop() error {
	for {
		line, err := client.reader.ReadString('\r')

		if err != nil {
			break
		}

		line = strings.TrimSpace(line)
		prefix, command, args := parseLine(line)
		message := Message{
			raw:     line,
			prefix:  prefix,
			command: command,
			args:    args,
		}

		fmt.Println(message.raw)

		client.Dispatch(command, message)

		if len(args) >= 2 && args[1][0] == '!' {
			s := strings.SplitN(args[1], " ", 2)
			message.args[1] = s[1]

			client.Dispatch(s[0], message)
		}
	}

	return nil
}

func (client *IRCClient) Privmsg(dst string, message string) error {
	fmt.Fprintf(client.connection, "PRIVMSG %s :%s\r\n", dst, message)

	return nil
}

func (client *IRCClient) Join(channel string) {
	var buffer bytes.Buffer

	if channel[0] != '#' {
		buffer.WriteByte('#')
	}

	buffer.WriteString(channel)

	log.Printf("joining channel: %s\n", buffer.String())

	client.AddHandler("001", func(client *IRCClient, message Message) {
		fmt.Fprintf(client.connection, "JOIN %s\r\n", buffer.String())
	})
}

func parseLine(line string) (string, string, []string) {
	var prefix string
	var args []string

	if line[0] == ':' {
		s := strings.SplitN(line[1:], " ", 2)
		prefix = s[0]
		line = s[1]
	}

	if strings.Contains(line, " :") {
		s := strings.SplitN(line, " :", 2)
		args = strings.Split(s[0], " ")
		args = append(args, s[1])
	} else {
		args = strings.Split(line, " ")
	}

	command := args[0]
	args = append(args[:0], args[1:]...)

	return prefix, command, args
}
