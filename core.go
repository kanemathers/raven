package main

import (
	"fmt"
	"math/rand"
)

func init() {
	RegisterModule("core", func() Module {
		return &Core{}
	})
}

type Core struct{}

func (module *Core) Init(client *IRCClient) error {
	client.AddHandler("PING", module.handlePing)
	client.AddHandler("CONNECTED", module.handleConnected)
	client.AddHandler("433", module.handleNickTaken)

	return nil
}

func (module *Core) handlePing(client *IRCClient, message Message) {
	fmt.Fprintf(client.connection, "PONG :%s\r\n", message.args[0])
}

func (module *Core) handleConnected(client *IRCClient, message Message) {
	fmt.Fprintf(client.connection, "USER raven 0 0 :raven\r\n")
	fmt.Fprintf(client.connection, "NICK raven\r\n")
}

func (module *Core) handleNickTaken(client *IRCClient, message Message) {
	fmt.Fprintf(client.connection, "NICK raven-%d\r\n", rand.Intn(999))
}
