package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	if len(os.Args) < 3 {
		fmt.Printf("usage: %s [hostname] [port]\n", os.Args[0])

		os.Exit(1)
	}

	port, err := strconv.Atoi(os.Args[2])

	if err != nil {
		fmt.Println("main: invalid port")

		os.Exit(1)
	}

	raven, err := NewClient()

	if err != nil {
		log.Fatal("main: failed to create client")
	}

	raven.LoadModules([]string{"core"})

	if err := raven.Connect(os.Args[1], port); err != nil {
		log.Fatalf("failed to connect to %s:%d\n", os.Args[1], port)
	}

	defer raven.Disconnect()

	if len(os.Args) >= 4 {
		for _, channel := range os.Args[3:] {
			raven.Join(channel)
		}
	}

	raven.Loop()
}
